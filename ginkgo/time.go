package ginkgo

import (
	"database/sql"
	"gitee.com/caincain/common-brick/lotus"
	"time"
)

func ToNullableTimeWithFormat(t interface{}, format ...string) sql.NullTime {
	if t == nil {
		return sql.NullTime{Valid: false}
	} else {
		if t.(string) == "" {
			return sql.NullTime{Valid: false}
		}
		for _, tf := range format {
			if timeParse, err := time.Parse(tf, t.(string)); err != nil {
				continue
			} else {
				return sql.NullTime{Time: timeParse, Valid: true}
			}
		}
		lotus.Error(t.(string))
		return sql.NullTime{Valid: false}
	}
}

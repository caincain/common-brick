package ginkgo

import (
	"database/sql"
	"strconv"
)

func ToNullableFloat64(f interface{}) sql.NullFloat64 {
	if f == nil {
		return sql.NullFloat64{Valid: false}
	} else {
		switch f.(type) {
		case float64:
			return sql.NullFloat64{Float64: f.(float64), Valid: true}
		case string:
			if f.(string) == "" {
				return sql.NullFloat64{Valid: false}
			}
			if _f, err := strconv.ParseFloat(f.(string), 64); err != nil {
				panic(err)
			} else {
				return sql.NullFloat64{Float64: _f, Valid: true}
			}
		default:
			panic("unknown type")
		}
	}
}

package ginkgo

import "encoding/json"

func ToJsonString(in interface{}) string {
	raw, _ := json.Marshal(&in)
	return string(raw)
}

func ToJsonIndentString(in interface{}) string {
	raw, _ := json.MarshalIndent(&in, "", "\t")
	return string(raw)
}

func ToJsonIndentBytes(in interface{}) []byte {
	raw, _ := json.MarshalIndent(&in, "", "\t")
	return raw
}

func ToJsonBytes(in interface{}) []byte {
	raw, _ := json.Marshal(&in)
	return raw
}

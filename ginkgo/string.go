package ginkgo

import (
	"database/sql"
	"fmt"
	"log"
	"reflect"
)

func ToKeyString(in interface{}) string {
	return fmt.Sprintf("%v", in)
}

func ToNullableString(in interface{}) sql.NullString {
	if in == nil {
		return sql.NullString{Valid: false}
	}
	switch in.(type) {
	case string:
		if in.(string) != "" {
			return sql.NullString{Valid: true, String: in.(string)}
		}
	case *string:
		if in.(*string) != nil {
			return sql.NullString{Valid: true, String: *in.(*string)}
		}
	default:
		log.Println(fmt.Sprintf("unknown input type %v", reflect.TypeOf(in)))
	}
	return sql.NullString{Valid: false}
}

package ginkgo

import (
	"database/sql"
	"fmt"
	"reflect"
	"strconv"
)

func ToInt64(in interface{}) (result int64) {
	switch in.(type) {
	case string:
		raw, err := strconv.ParseInt(in.(string), 10, 64)
		if err != nil {
			panic(fmt.Sprintf("fail to convert %s to int64", in))
		} else {
			result = raw
		}
	case int64:
		result = in.(int64)
	case int:
		result = int64(in.(int))
	case int32:
		result = int64(in.(int32))
	default:
		panic(fmt.Sprintf("unknown input type %v, value: %v", reflect.TypeOf(in), in))
	}
	return
}

func ToInt(in interface{}) (result int) {
	switch in.(type) {
	case string:
		raw, err := strconv.Atoi(in.(string))
		if err != nil {
			panic(fmt.Sprintf("fail to convert %s to int", in))
		} else {
			result = raw
		}
	case int64:
		result = int(in.(int64))
	case int:
		result = in.(int)
	case int32:
		result = int(in.(int32))
	default:
		panic(fmt.Sprintf("unknown input type %v, value: %v", reflect.TypeOf(in), in))
	}
	return
}
func ToNullableInt64(i interface{}) sql.NullInt64 {
	if i == nil {
		return sql.NullInt64{Valid: false}
	} else {
		switch i.(type) {
		case *int64:
			if i.(*int64) == nil {
				return sql.NullInt64{Valid: false}
			}
			return sql.NullInt64{Int64: *i.(*int64), Valid: true}
		case int64:
			return sql.NullInt64{Int64: i.(int64), Valid: true}
		case float64:
			return sql.NullInt64{Int64: int64(i.(float64)), Valid: true}
		case string:
			if i.(string) == "" {
				return sql.NullInt64{Valid: false}
			}
			if _i, err := strconv.ParseInt(i.(string), 10, 64); err != nil {
				panic(err)
			} else {
				return sql.NullInt64{Int64: _i, Valid: true}
			}
		default:
			panic("unknown type")
		}
	}
}

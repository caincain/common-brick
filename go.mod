module gitee.com/caincain/common-brick

go 1.16

require (
	go.uber.org/zap v1.19.1
	gorm.io/gorm v1.22.3
)

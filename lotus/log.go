package lotus

import (
	"fmt"
	"go.uber.org/zap"
	"go.uber.org/zap/zapcore"
	"sync"
	"time"
)

var (
	_log          *zap.Logger
	_LogSingleton sync.Once
)

func initializeLogger() *zap.Logger {
	var err error
	config := zap.Config{
		Level:       zap.NewAtomicLevelAt(zap.DebugLevel),
		Development: true,
		Encoding:    "console",
		EncoderConfig: zapcore.EncoderConfig{
			TimeKey:        "time",
			LevelKey:       "level",
			NameKey:        "log",
			CallerKey:      "caller",
			MessageKey:     "msg",
			StacktraceKey:  "trace",
			LineEnding:     zapcore.DefaultLineEnding,
			EncodeLevel:    zapcore.CapitalLevelEncoder,
			EncodeTime:     zapcore.TimeEncoderOfLayout("2006-01-02 15:04:05"),
			EncodeDuration: zapcore.SecondsDurationEncoder,
			EncodeCaller:   zapcore.ShortCallerEncoder,
		},
		OutputPaths:      []string{"stdout"},
		ErrorOutputPaths: []string{"stdout"},
	}
	if _log, err = config.Build(); err != nil {
		panic(fmt.Sprintf("base logger intialize fail: %v", err))
	} else {
		go func() {
			for {
				select {
				case <-time.After(time.Minute):
					_ = _log.Sync()
				}
			}
		}()
	}
	return _log
}

func Log() *zap.Logger {
	_LogSingleton.Do(func() { _log = initializeLogger() })
	return _log
}

func Info(msg string, fields ...zap.Field) {
	Log().Info(msg, fields...)
}

func Debug(msg string, fields ...zap.Field) {
	Log().Debug(msg, fields...)
}

func Warn(msg string, fields ...zap.Field) {
	Log().Warn(msg, fields...)
}

func Error(msg string, fields ...zap.Field) {
	Log().Error(msg, fields...)
}

func DPanic(msg string, fields ...zap.Field) {
	Log().DPanic(msg, fields...)
}

func Panic(msg string, fields ...zap.Field) {
	Log().Panic(msg, fields...)
}

func Fatal(msg string, fields ...zap.Field) {
	Log().Fatal(msg, fields...)
}

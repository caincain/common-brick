package plum

import "gitee.com/caincain/common-brick/ginkgo"

type baseFilter struct {
	filter map[string]bool
}

func NewFilter() Filter {
	return &baseFilter{filter: map[string]bool{}}
}

func (o *baseFilter) WithElement(ele ...interface{}) bool {
	var key string
	for _, v := range ele {
		key += ginkgo.ToKeyString(v)
	}
	if o.filter[key] {
		return false
	} else {
		o.filter[key] = true
		return true
	}
}

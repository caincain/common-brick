package plum

import (
	"fmt"
	"gitee.com/caincain/common-brick/ginkgo"
	"gitee.com/caincain/common-brick/lotus"
	"gorm.io/gorm"
	"gorm.io/gorm/clause"
	"gorm.io/gorm/schema"
)

type pgSinker struct {
	model      interface{}
	primaryKey []clause.Column
	columns    []string
	client     *gorm.DB
	handler    SinkErrorHandler
}

func NewPgSinker() Sinker {
	return &pgSinker{handler: func(sinker Sinker, err error, data interface{}) {
		lotus.Error(err.Error())
		fmt.Println(ginkgo.ToJsonIndentString(data))
		panic(err)
	},
	}
}

func (o *pgSinker) WithSinkErrorHandler(handler SinkErrorHandler) Sinker {
	o.handler = handler
	return o
}

func (o *pgSinker) WithClient(client interface{}) Sinker {
	o.client = client.(*gorm.DB)
	return o
}

func (o *pgSinker) WithModel(model interface{}) Sinker {
	o.model = model
	return o
}

func (o *pgSinker) WithPrimaryKey(primaryKey ...clause.Column) Sinker {
	o.primaryKey = primaryKey
	return o
}

func (o *pgSinker) WithAssignmentColumns(columns ...string) Sinker {
	o.columns = columns
	return o
}

func (o *pgSinker) Sink(data interface{}) {
	var tx = o.client.
		Model(o.model).
		Clauses(clause.OnConflict{
			Columns:   o.primaryKey,
			DoUpdates: clause.AssignmentColumns(o.columns)},
		).
		CreateInBatches(data, 100)
	if tx.Error != nil {
		o.handler(o, tx.Error, data)
		return
	} else {
		lotus.Info(fmt.Sprintf("sink %d rows to %s", tx.RowsAffected, o.model.(schema.Tabler).TableName()))
	}
}

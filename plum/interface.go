package plum

import "gorm.io/gorm/clause"

type Sinker interface {
	WithClient(client interface{}) Sinker
	WithModel(model interface{}) Sinker
	WithPrimaryKey(primaryKey ...clause.Column) Sinker
	WithAssignmentColumns(columns ...string) Sinker
	Sink(data interface{})
	WithSinkErrorHandler(handler SinkErrorHandler) Sinker
}
type SinkErrorHandler func(sinker Sinker, err error, data interface{})

type Filter interface {
	WithElement(elements ...interface{}) bool
}

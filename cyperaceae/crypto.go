package cyperaceae

import (
	"crypto/md5"
	"crypto/sha256"
	"crypto/sha512"
	"encoding/base64"
	"encoding/hex"
)

func Md5(in string) (out string) {
	m := md5.New()
	m.Write([]byte(in))
	return hex.EncodeToString(m.Sum(nil))
}

func Sha256(in string) (out string) {
	m := sha256.New()
	m.Write([]byte(in))
	return hex.EncodeToString(m.Sum(nil))
}

func Sha512(in string) (out string) {
	m := sha512.New()
	m.Write([]byte(in))
	return hex.EncodeToString(m.Sum(nil))
}

func Base64EncodeFromBytes(in []byte) (out string) {
	return base64.StdEncoding.EncodeToString(in)
}

func Base64DecodeFromString(in string) (out []byte, err error) {
	return base64.StdEncoding.DecodeString(in)
}
